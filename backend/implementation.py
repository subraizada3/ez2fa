#  Copyright (C) 2018 Subramaniyam Raizada
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.


# app.py does the Flask integration and rate limiting/authentication
# this file interacts with the DB file and performs the actual operations

import os
from config import *
from pyotp import TOTP, HOTP, random_base32
from threading import Lock
from typing import List


db_lock = Lock()

# messages to respond to HTTP requests with
SUCCESS = 'SUCCESS\n'
ERROR = 'ERROR\n'
NO_SUCH_SERVICE = 'NO SUCH SERVICE\n'
SERVICE_ALREADY_EXISTS = 'SERVICE ALREADY EXISTS\n'
NO_SERVICES = 'NO SERVICES\n'



# db file row format:
# name:secret:type:domain



def read_db_file() -> str:
	"Returns the contents of the database file as a string, or an empty string if no db file exists"
	result = ''
	db_lock.acquire()
	if os.path.isfile(DB_FILE_PATH):
		result = open(DB_FILE_PATH).read()
	db_lock.release()
	return result

def write_db_file(x: str) -> None:
	db_lock.acquire()
	# first, write new db to a tmp file
	with open(DB_FILE_PATH + '.tmp', 'w') as file:
		file.write(x)
	# then, mv the old db to a backup file
	if os.path.isfile(DB_FILE_PATH):
		os.rename(DB_FILE_PATH, DB_FILE_PATH + '.bak')
	# and mv the tmp file to the actual db file
	os.rename(DB_FILE_PATH + '.tmp', DB_FILE_PATH)
	# and finally, delete the old db file backup
	if os.path.isfile(DB_FILE_PATH + '.bak'):
		os.remove(DB_FILE_PATH + '.bak')
	db_lock.release()

def get_all_service_names() -> List[str]:
	"Returns list of all service names in DB file"
	file_str = read_db_file()
	result = []
	for line in file_str.splitlines():
		result.append(line.split(':')[0])
	return result

def get_otp_str_for_line(line: str, get_domain: bool) -> str:
	"""
	Returns a string of the format otp:counter, e.g. 123456:52 (counter is -1 for TOTP). Ends with \n.
	If get_domain is True, this will add the domain, if it exists, to the end of the line. E.g. 123456:52:google.com or 123456:-1: if no domain name

	:param line: database file line to generate OTP for
	:param get_domain: append domain of service to result?
	"""
	secret = line.split(':')[1]
	counter_str = line.split(':')[2]
	counter = int(counter_str)

	otp = TOTP(secret).now() if counter == -1 else HOTP(secret).at(counter)

	without_domain = otp + ':' + counter_str
	if get_domain:
		return without_domain + ':' + line.split(':')[3] + '\n'
	else:
		return without_domain + '\n'

def genotp():
	return random_base32() + '\n'


def add(name: str, secret: str, counter: int = -1, domain: str = '') -> str:
	"""
	Add a OTP service to the database.
	Returns SERVICE_ALREADY_EXISTS, or ERROR if any input is invalid.
	Otherwise, returns the same content as get_otp():
	SUCCESS on first line followed by otp:counter, e.g. 123456:123; counter is -1 for TOTP

	:param name: service name, as a string containing a-z 1-9 - _
	:param secret: OTP secret
	:param counter: counter for HOTP, -1 for TOTP
	:param domain: string of domain name for login page, optional
	"""
	if name in get_all_service_names(): return SERVICE_ALREADY_EXISTS

	# validate data
	if counter < -1: return ERROR
	if len(name) == 0: return ERROR
	# check that first/last letter of name are not in the disallowed characters
	if name[0] in INVALID_SERVICE_NAME_EDGE_CHARACTERS:
		return ERROR
	if name[len(name) - 1] in INVALID_SERVICE_NAME_EDGE_CHARACTERS:
		return ERROR
	# check that all characters in service name are valid
	for c in name:
		if c not in VALID_SERVICE_NAME_CHARACTERS:
			return ERROR
	# convert secret to uppercase and validate that it is only base32 characters
	secret = secret.upper()
	for c in secret:
		if c not in BASE32_CHARACTERS:
			return ERROR

	old_db: str = read_db_file()
	write_db_file(old_db + "{}:{}:{}:{}\n".format(name, secret, counter, domain))
	return get_otp(name)


def delete(name: str) -> str:
	"""
	Delete a OTP service from the database.
	Returns NO_SUCH_SERVICE or SUCCESS.

	:param name: service name to delete
	"""
	if name not in get_all_service_names(): return NO_SUCH_SERVICE

	old_db = read_db_file()
	new_db = ''
	# copy all lines from old_db into new_db, except for the one we don't want
	for line in old_db.splitlines():
		if not line.startswith(name + ':'):
			new_db += line + '\n'
	write_db_file(new_db)

	return SUCCESS


def get_otp(name: str) -> str:
	"""
	Get the OTP and counter (-1 for TOTP) for the given service name.
	Returns NO_SUCH_SERVICE or SUCCESS.
	On successful completion, returns SUCCESS on the first line of the reply and 123456:123 as the OTP:counter on the second line of the reply.

	:param name: service name to get OTP for
	"""
	if name not in get_all_service_names(): return NO_SUCH_SERVICE

	for line in read_db_file().splitlines():
		if line.startswith(name + ':'):
			return SUCCESS + get_otp_str_for_line(line, False)


def get_all_otps() -> str:
	"""
	Get a table of information about all services.
	Returns NO_SERVICES if zero services in database.
	Otherwise, returns SUCCESS followed by lines in this format:
	name:otp:counter:domain

	Examples:
	gmail:123456:-1:   (domain is blank)
	github:123456:52:github.com
	"""
	if len(get_all_service_names()) == 0: return NO_SERVICES

	result = SUCCESS
	for line in read_db_file().splitlines():
		result += get_otp_str_for_line(line, True)
	return result


def increment_hotp(name: str) -> str:
	"""
	Increments the HOTP counter for the given service.
	Returns NO_SUCH_SERVICE or SUCCESS with a single integer (counter) on the next line which is -1 for TOTP.

	:param name: service name to increment counter of
	"""
	if name not in get_all_service_names(): return NO_SUCH_SERVICE

	old_db = read_db_file()
	new_db = ''
	# copy all lines in old_db to new_db, but increment the counter for the
	#   service we are working on
	for line in old_db.splitlines():
		if not line.startswith(name + ':'):
			new_db += line + '\n'
		else:
			parts = line.split(':')
			old_counter = int(parts[2])
			if old_counter == -1: # this is a TOTP service, not HOTP
				new_db += line + '\n'
				new_counter = -1
				break
			else:
				new_counter = str(old_counter + HOTP_INCREMENT)
				new_db += parts[0] + ':' + parts[1] + ':' + new_counter + ':' + parts[3] + '\n'
				break

	write_db_file(new_db)
	return SUCCESS + str(new_counter) + '\n'


def reorder_services(order: str):
	"""
	Reorder list of services. POST data has param 'order": list of comma-separated numbers (new order).
	Returns SUCCESS if successful, otherwise ERROR.
	POST data list must start at 0, have the same number of elements as number of services, and must not skip any numbers.
	POST data may *not* contain a trailing comma.
	For example, a valid input if 4 services are registered could be '3,2,0,1'
	"""
	try:
		ints = [int(s) for s in order.split(',')] # list of integers (converted from strings)
	except:
		return ERROR
	# check that it has the right number of elements
	if len(ints) != len(get_all_service_names()): return ERROR
	# check that it has all the required elements (0, 1, 2, ...)
	for i in range(len(get_all_service_names())):
		if i not in ints: return ERROR

	old_lines = read_db_file().splitlines()
	new_db = ''
	for i in range(len(ints)):
		new_db += old_lines[ints[i]] + '\n'
	write_db_file(new_db)
	return SUCCESS


def get_all_data() -> str:
	"""
	Returns SUCCESS followed by the entire raw database file.
	Returns NO_SERVICES if zero services in database.
	Database file row format:
	name:secret:counter:domain
	counter is -1 for TOTP, domain may be empty (line will end with :)
	"""
	if len(get_all_service_names()) == 0: return NO_SERVICES
	return SUCCESS + read_db_file()
