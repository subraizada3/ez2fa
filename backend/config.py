# CONFIGURATION OPTIONS
# To change your password, run this file:
# $ python config.py

DB_FILE_PATH = "data.txt"

#PASSWORD: str = 'put_pw_here'
# Example: (this password is 'test')
PASSWORD: str = '$argon2i$v=19$m=102400,t=2,p=8$hdB6L6WUMgagVKpVqtW6lw$kWC6DQlVV1A2SGtbNhrYqQ'

# How much to increment the HOTP counter for that API call.
# Note that API clients should only increment the HOTP counter if they
# know that the generated OTP has actually been used. Otherwise, if one
# only infrequently logs in to a HOTP service but frequently fetches the
# list of all OTPs, the counter can increase beyond the 2FA server's window
# size and will be rejected.
# For clients that don't have a way to know if the generated OTP was used,
# show a reminder to the user to manually increment the counter after using
# a HOTP OTP.
HOTP_INCREMENT = 2

# Rate limiting is done by filling a bucket slightly for every bad login attempt.
# If the bucket fills up completely, it must fully drain before any more login
# attempts are accepted.
# MAX_ATTEMPTS is the number of units the bucket can hold.
# The bucket's load is decreased one unit after every COOLDOWN seconds.
RATE_LIMITING_MAX_ATTEMPTS: int = 4
RATE_LIMITING_COOLDOWN: int = 10

# you probably shouldn't change these...
# lowercase letters + numbers + dash + underscore
VALID_SERVICE_NAME_CHARACTERS =\
'abcdefghijklmnopqrstuvwxyz0123456789-_'
# first and last letter of a service name shouldn't be a dash or underscore
INVALID_SERVICE_NAME_EDGE_CHARACTERS = '-_'

# you definitely shouldn't change this
BASE32_CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'

if __name__ == '__main__':
	from passlib.hash import argon2
	from getpass import getpass # input() without echoing user input
	pw = getpass()
	print('Copy/paste the next line into the PASSWORD variable in config.py:')
	print(argon2.hash(pw))
