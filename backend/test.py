#!/usr/bin/env python

# Tests the API
# Assumes that the API is running at localhost:5000
# Does not test rate limiting

from config import *
from implementation import read_db_file

import os
from urllib.request import urlopen, Request
from urllib.error import HTTPError
from pyotp import random_base32

# change working directory to this script's folder
# https://stackoverflow.com/a/1432949
os.chdir(os.path.dirname(os.path.abspath(__file__)))

# clear the DB file before beginning the test
if os.path.isfile(DB_FILE_PATH): os.remove(DB_FILE_PATH)
assert read_db_file() == ''


PW = '?pw=' + PASSWORD
def curl(r: Request) -> str:
	"Execute the request and return the result as a string"
	return urlopen(r).read().decode('UTF-8')


# verify that password is checked - should return HTTP 401
try:
	print(urlopen('http://localhost:5000/api/otps/' + PW + 'not'))
	assert False
except HTTPError as e:
	if not e.code == 401:
		assert False



# add a TOTP service 'gmail' with domain 'google.com'
otp = random_base32()
data = ('name=gmail&secret=' + otp + '&domain=google.com').encode('UTF-8')
addYahoo = Request('http://localhost:5000/api/otps/' + PW, data=data)
output = curl(addYahoo)
assert output.splitlines()[0] == 'SUCCESS'
print('Valid OTP with counter = -1:\n{}'.format(output.splitlines()[1]))
assert len(read_db_file().splitlines()) == 1

# get all OTPs
getAllOtps = Request('http://localhost:5000/api/otps/' + PW)
output = curl(getAllOtps)
assert output.splitlines()[0] == 'SUCCESS'
assert output.splitlines()[1].split(':')[1] == '-1'
assert output.splitlines()[1].split(':')[2] == 'google.com' # domain was left empty

# add a HOTP service 'github' with domain 'github.com'
otp = random_base32()
counter = 0
data = ('name=github&secret=' + otp + '&counter=' + str(counter) + '&domain=github.com').encode('UTF-8')
addGithub = Request('http://localhost:5000/api/otps/' + PW, data=data)
output = curl(addGithub)
assert output.splitlines()[0] == 'SUCCESS'
print()
print('Valid OTP with counter = {}:\n{}'.format(counter, output.splitlines()[1]))
assert len(read_db_file().splitlines()) == 2

# increment github HOTP
addGithub = Request('http://localhost:5000/api/otps/github/' + PW, method='PUT')
output = curl(addGithub)
assert output.splitlines()[0] == 'SUCCESS'
assert output.splitlines()[1] == str(counter + HOTP_INCREMENT)

# add a TOTP service 'yahoo' with no domain and manual counter
otp = random_base32()
data = ('name=yahoo&secret=' + otp + '&counter=-1').encode('UTF-8')
addYahoo = Request('http://localhost:5000/api/otps/' + PW, data=data)
output = curl(addYahoo)
assert output.splitlines()[0] == 'SUCCESS'
print()
print('Valid OTP with counter = -1:\n{}'.format(output.splitlines()[1]))
assert len(read_db_file().splitlines()) == 3

# delete the 'gmail' service
deleteGmail = Request('http://localhost:5000/api/otps/gmail/' + PW, method='DELETE')
assert curl(deleteGmail) == 'SUCCESS\n'
assert len(read_db_file().splitlines()) == 2

# try to delete the 'fake' service, should give NO SUCH SERVICE
deleteFake = Request('http://localhost:5000/api/otps/fake/' + PW, method='DELETE')
assert curl(deleteFake) == 'NO SUCH SERVICE\n'

# try to increment the 'yahoo' service, which is TOTP, counter should stay as -1
addYahoo = Request('http://localhost:5000/api/otps/yahoo/' + PW, method='PUT')
assert curl(addYahoo) == 'SUCCESS\n-1\n'

# try to increment the 'gmail' service, which we deleted
addGmail = Request('http://localhost:5000/api/otps/gmail/' + PW, method='PUT')
assert curl(addGmail) == 'NO SUCH SERVICE\n'

# try to get OTP for the 'fake' service, should give NO SUCH SERVICE
getFake = Request('http://localhost:5000/api/otps/fake/' + PW)
assert curl(getFake) == 'NO SUCH SERVICE\n'

# get OTP for the 'github' service
getGithub = Request('http://localhost:5000/api/otps/github/' + PW)
output = curl(getGithub)
assert output.splitlines()[0] == 'SUCCESS'
print()
print('Valid OTP with counter >0:') # github was incremented earlier
print(output.splitlines()[1])

# get all otps (github, yahoo)
getAll = Request('http://localhost:5000/api/otps/' + PW)
output = curl(getAll)
assert output.splitlines()[0] == 'SUCCESS'
assert len(output.splitlines()) == 3
print()
print('Two OTPs, one with counter -1 and domain github.com, one with counter >0 and no domain:')
print(output.splitlines()[1])
print(output.splitlines()[2])

# get data
getData = Request('http://localhost:5000/api/download/' + PW)
output = curl(getData)
assert output.splitlines()[0] == 'SUCCESS'
remainder = ''
for i in range(1, len(output.splitlines())):
	remainder += output.splitlines()[i] + '\n'
assert remainder == read_db_file()

# re-add yahoo, should give SERVICE ALREADY EXISTS
data = ('name=yahoo&secret=' + otp + '&counter=-1').encode('UTF-8')
addYahoo = Request('http://localhost:5000/api/otps/' + PW, data=data)
assert curl(addYahoo) == 'SERVICE ALREADY EXISTS\n'
assert len(read_db_file().splitlines()) == 2

# rearrange files, should give ERROR with first few examples, then SUCCESS
data = ('order=0,1,2').encode('UTF-8')
reorder = Request('http://localhost:5000/api/reorder/' + PW, data=data)
assert curl(reorder) == 'ERROR\n'
data = ('order=1,2').encode('UTF-8')
reorder = Request('http://localhost:5000/api/reorder/' + PW, data=data)
assert curl(reorder) == 'ERROR\n'
data = ('order=1,0').encode('UTF-8')
reorder = Request('http://localhost:5000/api/reorder/' + PW, data=data)
assert curl(reorder) == 'SUCCESS\n'
# now get list of services, should be in the opposite order of what we previously printed
getAll = Request('http://localhost:5000/api/otps/' + PW)
output = curl(getAll)
assert output.splitlines()[0] == 'SUCCESS'
assert len(output.splitlines()) == 3
print()
print('Same two OTPs as before, but in reverse order:')
print(output.splitlines()[1])
print(output.splitlines()[2])


# delete all services
delete = Request('http://localhost:5000/api/otps/github/' + PW, method='DELETE')
assert curl(delete) == 'SUCCESS\n'
delete = Request('http://localhost:5000/api/otps/yahoo/' + PW, method='DELETE')
assert curl(delete) == 'SUCCESS\n'

assert read_db_file() == ''

getAll = Request('http://localhost:5000/api/otps/' + PW)
assert curl(getAll) == 'NO SERVICES\n'
getData = Request('http://localhost:5000/api/download/' + PW)
assert curl(getData) == 'NO SERVICES\n'

# check ping endpoint
ping = Request('http://localhost:5000/api/ping/' + PW + 'x')
try:
	curl(ping)
	assert False
except HTTPError as e:
	if not e.code == 401:
		assert False
ping = Request('http://localhost:5000/api/ping/' + PW)
assert curl(ping) == 'SUCCESS\n'

print('\nAll tests passed!')
