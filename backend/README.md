# ez2fa API and REST backend

Table of contents:
* About the backend and 2FA
* API documentation
* Deploying on a server

### TODO: deployment on server

# About the backend and 2FA
The backend manages all the 2FA secret codes, which are necessary to generate one time pins (OTPs). There are two main algorithms 2FA algorithms in use, TOTP and HOTP (details below), and while TOTP allows you to share one secret code among all the 2FA provider devices (phones, etc.), HOTP requires a separate secret for each device, or for the devices to somehow keep a counter in sync (which the backend provides).

The backend allows you to put your 2FA secret codes in one place, and clients (your phone, computer, etc.) can ask it for the current OTP to login with. This simplifies the process of enrolling in 2FA (you only need to add the secret key once, instead of copying it to all your devices) and keeps HOTP counters in sync, which reduces the complexity of your 2FA solution. Clients can ask the backend for a OTP, add or delete a 2FA service (secret), and can easily generate a backup of the 2FA codes which you can store, in case the server the backend is hosted on fails.

The backend stores a list of 2FA services in a plain text file database. Each service has a name, secret, counter, and optionally a domain. The counter is ignored for TOTP, which does not use it. The domain name is optional and can be used by clients to autofill OTPs into a login page.

If offline, a client with the database downloaded from the server can use TOTP normally, and can use HOTP as long as the counter is increased on the backend before trying to use the HOTP keys the backend generates.

The backend is licensed under the GNU AGPL. See `LICENSE.txt` for details.

It has minimal dependencies: Python 3, and `pip install flask pyotp`.

### TOTP/HOTP
There are two main 2FA algorithms; both work by doing some black magic (math) on a combination of numbers. For TOTP (time-based OTP), the two numbers are a secret string and the current time. For HOTP, the two numbers are a secret string and a increasing counter.

TOTP is commonly used; HOTP is used mainly for devices that cannot reliably tell the time, such as hardware push-button tokens that give you a new key whenever you push the button.

With TOTP, you can put the secret code in all your devices and they will generate the same OTPs. This will not work with HOTP, because the 2FA server will check that the counter has increased. So if one device uses a OTP, each other device needs to know that the counter has increased. The practical solution is to give a different secret code to each device, so they can all maintain their own counters.

With HOTP, it is also possible for some counters to be skipped. If a device has counter = 52, you can press the new code button a few times to get the code with counter = 58. The server knows the last counter you used was 51, so it will check whether the code you give matched for counter 52, 53, 54, ..., until it finds a match at counter = 58. However, it will only do this a limited number of times, called the window size. If you refresh the HOTP code a few hundred times, the server will deny your code because it did not match any of the counters that it checked in the window, and you will need to generate a new secret code and update your devices. The backend effectively allows all your 2FA devices to share one secret code and counter, removing this complexity.

# API Documentation
The API root is `/api/otps/` for all operations that involve OTPs. Include the trailing slash. Administrative options such as making a data backup or reordering the list of services will specify their path.

All API methods take a password, and requests with an incorrect password are rate limited. The password should be given as a the URL parameter `pw`, e.g. `/api/otps/?pw=your_password`.

Aside from a HTTP 429 (rate limited) or 401 (incorrect password), all methods return HTTP 200 with *plain* text as their data (no JSON or other newfangled crap). All responses consist of whole lines (characters terminated by a `\n`), so when I state that a request will return `ERROR`, it means it will return `ERROR\n`.

### Data limitations
Secret keys must be base32 encoded (you should get them in this format), which means they may contain only the letters A-Z and the numbers 2-7. The API will convert lowercase secrets to uppercase.

Names can contains the letters defined in `config.py`. By default, this means they may contain **lowercase** letters, numbers, dash (`-`), and underscore (`_`). The first and last characters of a name cannot be dash or underscore.

### Methods
* add (POST): add a new OTP service to the backend, provided its name and secret
* delete (DELETE): delete a OTP service from the backend
* get OTP (GET): get the OTP and counter for a service (by name)
* get all OTPs (GET): get the OTP, counter, and domain for all services
* increment (PUT): increment the counter for a HOTP service; returns the new counter
* reorder services (POST): change order of added services
* download data (GET): get a copy of the backend's database file
* ping (GET): requires password to ping, can be used to verify password correctness
* generate secret (GET): make a random secret code (for TOTP or HOTP)

## Add
POST to the API root. The POST data should be in the same format as URL parameters. The data must contain `name`, which is the name of the new service to add, and `secret`, which is the secret (base32 encoded, but you shouldn't have to manually do that).

For HOTP, add the `counter` parameter, for the initial counter value. This should be at least `0`. For TOTP, omit it or set it to `-1`.

Finally, you can optionally add the `domain` parameter, which should be the domain of the login page of the service. For example, this would be `google.com` for Gmail and YouTube, since the login page is hosted at `google.com`.

Returns 'SERVICE ALREADY EXISTS' if a service with the given name already exists. Returns 'ERROR' if any of the provided data is invalid (secret is not base32, counter is < -1, or name has invalid characters). Otherwise, this will return 'SUCCESS', and a second line containing the result of the get OTP key method (which is `otp:counter`, e.g. `123456:-1` for TOTP or `123456:52` for HOTP).

Examples:
```bash
curl -d 'name=gmail&secret=ZV22566LIMSUM2TM&domain=google.com' http://localhost:5000/api/otps/?pw=test
curl -d 'name=github&secret=ZV22566LIMSUM2TM&counter=2' http://localhost:5000/api/otps/?pw=test
curl -d 'name=yahoo&secret=ZV22566LIMSUM2TM' http://localhost:5000/api/otps/?pw=test
curl -d 'name=bitbucket&secret=ZV22566LIMSUM2TM&counter=52&domain=id.atlassion.com' http://localhost:5000/api/otps/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

data = ('name=gmail&secret=ABCDEEF234567&domain=google.com').encode('UTF-8')
req = Request('http://localhost:5000/api/otps/?pw=test', data=data)
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
SERVICE ALREADY EXISTS
----------
ERROR
----------
SUCCESS
547327:482
```

## Delete
DELETE to the API root plus the service name. This will delete the service from the backend.

Returns 'NO SUCH SERVICE' or 'SUCCESS'.

Example:
```bash
curl -X DELETE http://localhost:5000/api/otps/gmail/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/otps/gmail/?pw=test', method='DELETE')
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
NO SUCH SERVICE
----------
SUCCESS
```

## Get OTP
GET to the API root plus the service name. May return 'NO SUCH SERVICE'.

Otherwise, returns 'SUCCESS', with a second line containg the current OTP and counter, separated by a colon. For TOTP, the counter is always -1.

Example
```bash
curl http://localhost:5000/api/otps/gmail/?pw=test
```

Python example
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/otps/gmail/?pw=test')
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
NO SUCH SERVICE
----------
SUCCESS
324752:-1
----------
SUCCESS
5738243:83
```

## Get all OTPs
GET to the API root. May return 'NO SERVICES' if the backend database is empty (contains no services).

Otherwise, returns 'SUCCESS', with one extra line for each service. Each service line is formatted as with the Get OTP method (see above), but also contain a third field for the domain, which is also separated by a semicolon and may be empty.

The response is in the same order as in the backend's database. You can change the order with the rearrange method.

Example
```bash
curl http://localhost:5000/api/otps/?pw=test
```

Python example
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/otps/?pw=test')
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
NO SERVICE
----------
SUCCESS
457383:-1:github.com
367822:82:id.atlassian.com
463727:-1:
437843:3724:
436724:-1:google.com
```

## Increment HOTP
PUT to the API root plus the service name. May return 'NO SUCH SERVICE'.

Otherwise, returns 'SUCCESS' with a second line containing the new counter. If called on a TOTP service, it will return a success but the counter will still be -1. The amount to increase the counter by can be changed in `config.py`.

Example:
```bash
curl -X PUT http://localhost:5000/api/otps/gmail/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/otps/gmail/?pw=test', method='PUT'
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
NO SUCH SERVICE
----------
SUCCESS
57
----------
SUCCESS
-1
```

## Reorder services
POST to `/api/reorder/`. The order that services are added in is kept and is used when getting the list of all OTPs. You can specify a new order for the services with this method.

POST data has a single parameter `order`, with its data being a comma-separated list of integers. The list may **not** end with a trailing comma. The list must start at 0, have as many elements as the list of services, and may not skip any numbers.

Returns 'ERROR' if the list is not valid, otherwise 'SUCCESS'.

The examples below assume that there are four services registered. If they are named A, B, C, and D (added in that order), this will reorder them to D, C, A, B.

Example:
```bash
curl -d 'order=3,2,0,1' http://localhost:5000/api/reorder/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

data = ('order=3,2,0,1').encode('UTF-8')
req = Request('http://localhost:5000/api/reorder/?pw=test', data=data)
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
ERROR
----------
SUCCESS
```

## Download database
GET to `/api/download/` (not the API root). Returns 'NO SERVICES' if the database is empty. Otherwise, returns 'SUCCESS' followed by an exact copy of the database file on the server.

Database row format is `name:secret:counter:domain`.

Example:
```bash
curl http://localhost:5000/api/download/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/download/?pw=test')
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
NO SERVICES
----------
SUCCESS
gmail:secretCodeHere:-1:google.com
github:secretCodeHere:4738:
bitbucket:secretCodeHere:-1:
yahoo:secretCodeHere:7:yahoo.com
```

## Ping
GET to `/api/ping/`. Returns 'SUCCESS'; incorrect password will give a HTTP 401 as with all API endpoints.

Example:
```bash
curl http://localhost:5000/api/ping/?pw=test
```

Python example:
```python
from urllib.request import urlopen, Request

req = Request('http://localhost:5000/api/ping/?pw=test')
result: str = urlopen(req).read().decode('UTF-8')
```

Sample output:
```
SUCCESS
```

## Generate secret
GET to `/api/genotp/`. Added for internal testing. Is password protected / rate limited. Does not operate on the normal API root. Secret can be used for TOTP or HOTP (only difference between the algorithm is that one adds the time to the secret, the other adds the counter).

Example:
```bash
curl http://localhost:5000/api/genotp/?pw=test
```

Output:
```
FVPSRTI55SDA2DUM
```

# Deploying on a server

# TODO

The only dependencies are Python 3+, and `sudo pip install flask pyotp passlib argon2-cffi`.

`Flask` is the internal webserver used (put a reverse proxy such as Nginx in front of this); `pyotp` is used to turn the 2FA secrets into the current OTP, and `passlib` is used to hash your password. `argon2-cffi` is the library to connect `passlib` with the argon2 hash implementation.

TODO: make password string by running config.py
