#!/usr/bin/env python3

#  Copyright (C) 2018 Subramaniyam Raizada
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.



from implementation import * # this imports the config file

from flask import request, abort, Flask
from passlib.hash import argon2
from functools import wraps
from threading import Lock, Timer
from werkzeug.datastructures import MultiDict
import os

# change working directory to this script's folder
# https://stackoverflow.com/a/1432949
os.chdir(os.path.dirname(os.path.abspath(__file__)))



########################################
###   RATE LIMITING
########################################
ratelimit_lock: Lock = Lock()
limit_buffer: int = 0
in_cooldown: bool = False # force cooldown to 0?

def verify_pw(pw: str) -> str:
	return argon2.verify(pw, PASSWORD) # PASSWORD is from config file

# function decorator
# https://coderwall.com/p/4qickw/require-an-api-key-for-a-route-in-flask-using-only-a-decorator
def increase_ratelimit_count_and_abort():
	global limit_buffer
	ratelimit_lock.acquire()
	limit_buffer += 1
	ratelimit_lock.release()
	abort(401)

def ratelimit(view_function):
	@wraps(view_function)
	def decorated_function(*args, **kwargs):
		global limit_buffer, in_cooldown

		# if max attempts reached, enter cooldown mode and deny request
		if limit_buffer >= RATE_LIMITING_MAX_ATTEMPTS or in_cooldown:
			# enter cooldown mode if at max attempts
			ratelimit_lock.acquire(); in_cooldown = True; ratelimit_lock.release()
			abort(429)

		# if the password is good, perform the request
		if request.args.get('pw') and verify_pw(request.args.get('pw')):
			return view_function(*args, **kwargs)
		else: # otherwise, increase failed attempts and deny the request
			increase_ratelimit_count_and_abort()

	return decorated_function

# rate limiting cooldown
def reduce_ratelimit_count():
	global limit_buffer, in_cooldown
	ratelimit_lock.acquire()
	if limit_buffer > 0: # decrement limit buffer
		limit_buffer -= 1
	if limit_buffer == 0: # exit cooldown mode once we get to 0
		in_cooldown = False
	ratelimit_lock.release()
	Timer(RATE_LIMITING_COOLDOWN, reduce_ratelimit_count).start()

# start the initial timer to reduce rate limit buffer
Timer(RATE_LIMITING_COOLDOWN, reduce_ratelimit_count).start()



########################################
###   FLASK PATHS
########################################
# this segment validates data and then calls then either returns an error or
#   calls the appropriate helper function to query/update the db
app = Flask(__name__)

# add OTP service
# curl -d 'name=gmail&secret=lol' http://localhost:5000/api/otps/?pw=test
#   counter defaults to -1, domain defaults to ''
@app.route('/api/otps/', methods=['POST'])
@ratelimit
def flask_add_otp():
	data: MultiDict = request.form
	name: str = data.get('name', '', str)
	secret: str = data.get('secret', '', str)
	counter: int = data.get('counter', -1, int)
	domain: str = data.get('domain', '', str)
	return add(name, secret, counter, domain)

# delete OTP service
# curl -X DELETE http://localhost:5000/api/otps/gmail/?pw=test
@app.route('/api/otps/<string:name>/', methods=['DELETE'])
@ratelimit
def flask_delete_otp(name: str):
	return delete(name)

# get OTP for service
# curl http://localhost:5000/api/otps/gmail/?pw=test
@app.route('/api/otps/<string:name>/', methods=['GET'])
@ratelimit
def flask_get_otp(name: str):
	return get_otp(name)

# get OTPs for all services
# curl http://localhost:5000/api/otps/?pw=test
@app.route('/api/otps/', methods=['GET'])
@ratelimit
def flask_get_all_otps():
	return get_all_otps()

# increment counter for HOTP
# curl -X PUT http://localhost:5000/api/otps/gmail/?pw=test
@app.route('/api/otps/<string:name>/', methods=['PUT'])
@ratelimit
def flask_increment_hotp(name: str):
	return increment_hotp(name)

# reorder list of services
# curl -d 'order=3,2,0,1' http://localhost:5000/api/reorder/?pw=test
@app.route('/api/reorder/', methods=['POST'])
@ratelimit
def flask_reorder_services():
	return reorder_services(request.form['order'])

# download all data
# curl http://localhost:5000/api/download/?pw=test
@app.route('/api/download/', methods=['GET'])
@ratelimit
def flask_download_data():
	return get_all_data()

# generate a random OTP
# mainly used for internal testing, so still requires authentication and is
#   rate limited to prevent abuse of this on a production server
@app.route('/api/genotp/', methods=['GET'])
@ratelimit
def flask_genotp():
	return genotp()


# ping the server to check password
@app.route('/api/ping/', methods=['GET'])
@ratelimit # this checks password
def flask_ping():
	return SUCCESS


########################################
###   MAIN()
########################################
if __name__ == '__main__':
	app.run(debug=True)
