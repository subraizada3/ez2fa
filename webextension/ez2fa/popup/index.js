var btn_loginShowHide = document.getElementById("btn_login_show_hide");
var btn_verifyLogin = document.getElementById("btn_login_update");
var div_loginContainer = document.getElementById("login_area");

btn_loginShowHide.addEventListener('click', toggleLoginContainerVisibility);
btn_verifyLogin.addEventListener('click', verifyLogin);

function toggleLoginContainerVisibility() {
	if (div_loginContainer.style.display === "none")
		div_loginContainer.style.display = "block";
	else div_loginContainer.style.display = "none";
}

function verifyLogin() {
	// TODO
}
